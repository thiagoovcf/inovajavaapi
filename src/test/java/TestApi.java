import com.google.gson.Gson;
import entidade.UserResponse;
import entidade.UserResquest;
import io.restassured.http.ContentType;
import io.restassured.module.jsv.JsonSchemaValidator;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import  io.restassured.RestAssured;

import java.io.File;

public class TestApi {


    @BeforeAll
    public static void beforeall(){
        RestAssured.baseURI = "https://reqres.in";
    }
    @Test
    void methodGet() {
        File file = new File("src/test/resources/schemaUser.json");

        RestAssured.given()
            .contentType(ContentType.JSON)
        .when()
            .get("api/users/2")
        .then().contentType(ContentType.JSON)
            .statusCode(200)
                .and().body(JsonSchemaValidator.matchesJsonSchema(file));
    }



    @Test
    public void testeThiagoGet(){

        RestAssured.given().contentType(ContentType.JSON)
                .when().get("api/users/{id}",3)
                .then().statusCode(HttpStatus.SC_OK).log().all();
    }
    @Test
    public void createUser(){
        String json = "{\n" +
                "    \"name\": \"morpheus\",\n" +
                "    \"job\": \"leader\"\n" +
                "}";
        RestAssured.given().contentType(ContentType.JSON).log().all()
                .body(json).when().post("api/users")
                .then().statusCode(HttpStatus.SC_CREATED).log().all();
    }
    @Test
    public void createUser2(){
        Gson gson = new Gson();
        UserResquest userRequest = new UserResquest("Thiago", "QA");
        String name = RestAssured.given()
                .contentType(ContentType.JSON).log().all()
                .body(gson.toJson(userRequest))
                .when().post("api/users")
                .then()
                .statusCode(HttpStatus.SC_CREATED).log().all()
                .and().extract().response().path("name");

        Assertions.assertEquals(name,"joao");
    }
    @Test
    public void createUser3(){
        Gson gson = new Gson();
        UserResquest userRequest = new UserResquest("Thiago", "QA");
        UserResponse userResponse = RestAssured.given()
                .contentType(ContentType.JSON).log().all()
                .body(gson.toJson(userRequest))
                .when().post("api/users")
                .then()
                .statusCode(HttpStatus.SC_CREATED).log().all()
                .and().extract().response().as(UserResponse.class);

        Assertions.assertNotNull(userResponse.getId());

    }

    @Test
    public void testePut(){
        UserResquest userResquest = new UserResquest("Henrique", "Manager");
        RestAssured.given().contentType(ContentType.JSON)
                .body(userResquest).when().put("api/users/2")
                .then().statusCode(HttpStatus.SC_OK);
    }

}
